import re

def getData(filename):

    with open(filename, "r") as f:
        lines = f.readlines()

    pinfo = lines[1].strip('\n')
    payoff = lines[3].strip('\n').split()
    payoff = map(float, payoff)

    min_payoff = min(payoff)
    if min_payoff < 0:
        payoff = [x-min_payoff for x in payoff]

    _, names, actions = pinfo.split('{')

    names = names.strip().strip('}').strip()
    names = re.findall(r'"(.*?)"', names)

    actions  = actions.strip().strip('}').strip().split()
    actions = map(int, actions)

    return names, actions, payoff
