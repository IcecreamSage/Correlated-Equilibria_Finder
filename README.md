## Introduction to Game Theory Programming Assignment 2

### Team:
- Kulin Shah Nitin Kumar : 201501234
- Moin Hussain Moti : 201501066

### Instructions
- Run the Code
`python main.py`
- When asked for input, give the filepath
`Provide the input : ${filepath}`

### Input Format
Standard NFG Format

### Output Format
Probability of playing various strategies in the same order as recieved from the NFG Format 